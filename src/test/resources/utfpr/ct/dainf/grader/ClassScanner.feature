Feature: Avalia a Prática 4.2 de IF62C-Fundamentos de Programação 2
    Como professor da disciplica de Fundamentos de Programação 2
    Quero avaliar a prática 4.2
    A fim de verificar a compreensão dos estudantes

    Background:
        Given the maximum grade is 200
        Given the main class is 'Avaliacao1'
        Given I set the script timeout to 3000
        Given I evaluate 'import utfpr.ct.dainf.if62c.avaliacao.*'
        Given I evaluate 'import utfpr.ct.dainf.grader.*'
        Given I evaluate 'import java.util.regex.*'
        # variáveis auxiliares para testar os métodos
        Given I evaluate 'double a1 = TestUtils.random(-3.0, 3.0)'
        Given I evaluate 'double b1 = TestUtils.random(-3.0, 3.0)'
        Given I evaluate 'double a2 = TestUtils.random(-3.0, 3.0)'
        Given I evaluate 'double b2 = TestUtils.random(-3.0, 3.0)'
        Given I evaluate 'double a3 = TestUtils.random(-3.0, 3.0)'
        Given I evaluate 'double b3 = TestUtils.random(-3.0, 3.0)'
    
    Scenario: Verifica se o construtor Complexo(double, double) está corretamente implementado. (10 pontos)
        Given I report 'Iniciando avaliação...'
        Given I report 'Avaliando item 2...'
        Given class 'utfpr.ct.dainf.if62c.avaliacao.Complexo' exists store class in <complexoClass>
        And class <complexoClass> has 'public' modifier
        And I evaluate 'Complexo C1 = new Complexo(a1, b1)'
        Then award 5 points
        Given I get field 'real' value in super class of <C1> save in <real1>
        Given I get field 'img' value in super class of <C1> save in <img1>
        And expression 'real1' evaluates to <a1>
        And expression 'img1' evaluates to <b1>
        Then award 5 points

    Scenario: Verifica se o método getReal() está corretamente implementado. (5 pontos)
        Given I report 'Avaliando item 3...'
        Given class <complexoClass> declares 'getReal()' method save in <getrealMethod>
        And member <getrealMethod> has 'public' modifier
        And method <getrealMethod> returns type 'double'
        And I evaluate 'Complexo C2 = new Complexo()'
        And I set field 'real' in <C2> to <a1> 
        And expression 'C2.getReal()' evaluates to <a1>
        Then award 5 points

    Scenario: Verifica se o método getImg() está corretamente implementado. (5 pontos)
        Given class <complexoClass> declares 'getImg()' method save in <getimgMethod>
        And member <getimgMethod> has 'public' modifier
        And method <getimgMethod> returns type 'double'
        And I evaluate 'Complexo C3 = new Complexo()'
        And I set field 'img' in <C3> to <b1> 
        And expression 'C3.getImg()' evaluates to <b1>
        Then award 5 points

    Scenario: Verifica se o método sub(Complexo) está corretamente implementado. (20 pontos)
        Given I report 'Avaliando item 4...'
        Given class <complexoClass> declares 'sub(utfpr.ct.dainf.if62c.avaliacao.Complexo)' method save in <subMethod>
        And member <subMethod> has 'public' modifier
        And method <subMethod> returns type 'utfpr.ct.dainf.if62c.avaliacao.Complexo'
        Then award 5 points
        Given I evaluate 'ComplexoTest CT1 = new ComplexoTest(a1, b1)'
        Given I evaluate 'ComplexoTest CT2 = new ComplexoTest(a2, b2)'
        Given I evaluate 'ComplexoTest CT3 = CT1.sub(CT2)'
        And I evaluate 'Complexo C4 = new Complexo(a1, b1)'
        And I evaluate 'Complexo C5 = new Complexo(a2, b2)'
        And I evaluate 'Complexo C6 = C4.sub(C5)'
        And I get field 'real' value in super class of <C6> save in <real2>
        And I get field 'img' value in super class of <C6> save in <img2>
        And expression 'TestUtils.equalsAprox(CT3.getReal(), real2, 1e-6)' evaluates to <true>
        And expression 'TestUtils.equalsAprox(CT3.getImg(), img2, 1e-6)' evaluates to <true>
        Then award 15 points

    Scenario: Verifica se o método prod(double) está corretamente implementado. (20 pontos)
        Given I report 'Avaliando item 5...'
        Given class <complexoClass> declares 'prod(double)' method save in <proddMethod>
        And member <proddMethod> has 'public' modifier
        And method <proddMethod> returns type 'utfpr.ct.dainf.if62c.avaliacao.Complexo'
        Then award 5 points
        Given I evaluate 'ComplexoTest CT4 = new ComplexoTest(a1, b1)'
        Given I evaluate 'ComplexoTest CT5 = CT4.prod(a3)'
        And I evaluate 'Complexo C7 = new Complexo(a1, b1)'
        And I evaluate 'Complexo C8 = C7.prod(a3)'
        And I get field 'real' value in super class of <C8> save in <real3>
        And I get field 'img' value in super class of <C8> save in <img3>
        And expression 'TestUtils.equalsAprox(CT5.getReal(), real3, 1e-6)' evaluates to <true>
        And expression 'TestUtils.equalsAprox(CT5.getImg(), img3, 1e-6)' evaluates to <true>
        Then award 15 points

    Scenario: Verifica se o método prod(Complexo) está corretamente implementado. (20 pontos)
        Given I report 'Avaliando item 6...'
        Given class <complexoClass> declares 'prod(utfpr.ct.dainf.if62c.avaliacao.Complexo)' method save in <prodcMethod>
        And member <prodcMethod> has 'public' modifier
        And method <prodcMethod> returns type 'utfpr.ct.dainf.if62c.avaliacao.Complexo'
        Then award 5 points
        Given I evaluate 'ComplexoTest CT6 = new ComplexoTest(a1, b1)'
        Given I evaluate 'ComplexoTest CT7 = new ComplexoTest(a2, b2)'
        Given I evaluate 'ComplexoTest CT8 = CT6.prod(CT7)'
        Given I evaluate 'Complexo C9 = new Complexo(a1, b1)'
        Given I evaluate 'Complexo C10 = new Complexo(a2, b2)'
        Given I evaluate 'Complexo C11 = C9.prod(C10)'
        And I get field 'real' value in super class of <C11> save in <real4>
        And I get field 'img' value in super class of <C11> save in <img4>
        And expression 'TestUtils.equalsAprox(CT8.getReal(), real4, 1e-6)' evaluates to <true>
        Then award 7 points

    Scenario: Verifica se a parte imaginária está correta.
        Given expression 'TestUtils.equalsAprox(CT8.getImg(), img4, 1e-6)' evaluates to <true>
        Then award 8 points

    Scenario: Verifica se o método div(Complexo) está corretamente implementado. (30 pontos)
        Given I report 'Avaliando item 7...'
        Given class <complexoClass> declares 'div(utfpr.ct.dainf.if62c.avaliacao.Complexo)' method save in <divMethod>
        And member <divMethod> has 'public' modifier
        And method <divMethod> returns type 'utfpr.ct.dainf.if62c.avaliacao.Complexo'
        Then award 5 points
        Given I evaluate 'ComplexoTest CT12 = new ComplexoTest(a1, b1)'
        Given I evaluate 'ComplexoTest CT13 = new ComplexoTest(a2, b2)'
        Given I evaluate 'ComplexoTest CT14 = CT12.div(CT13)'
        Given I evaluate 'Complexo C12 = new Complexo(a1, b1)'
        Given I evaluate 'Complexo C13 = new Complexo(a2, b2)'
        Given I evaluate 'Complexo C14 = C12.div(C13)'
        And I get field 'real' value in super class of <C14> save in <real5>
        And I get field 'img' value in super class of <C14> save in <img5>
        And expression 'TestUtils.equalsAprox(CT14.getReal(), real5, 1e-6)' evaluates to <true>
        Then award 12 points

    Scenario: Verifica se a parte imaginária está correta.
        And expression 'TestUtils.equalsAprox(CT14.getImg(), img5, 1e-6)' evaluates to <true>
        Then award 13 points

    Scenario: Verifica se o método sqrt() está corretamente implementado. (40 pontos)
        Given I report 'Avaliando item 8...'
        Given class <complexoClass> declares 'sqrt()' method save in <sqrtMethod>
        And member <sqrtMethod> has 'public' modifier
        And method <sqrtMethod> returns type '[Lutfpr.ct.dainf.if62c.avaliacao.Complexo;'
        Then award 5 points
        Given I evaluate 'ComplexoTest CT15 = new ComplexoTest(a1, b1)'
        Given I evaluate 'ComplexoTest[] CT16 = CT15.sqrt()'
        And I evaluate 'Complexo C15 = new Complexo(a1, b1)'
        And I evaluate 'Complexo[] C16 = C15.sqrt()'
        And expression 'C16.length' evaluates to <2>
        Then award 5 points

    Scenario: Verifica se a primeira raiz está correta.
        Given I evaluate 'Complexo C17 = C16[0]'
        And I get field 'real' value in super class of <C17> save in <real7>
        And I get field 'img' value in super class of <C17> save in <img7>
        And expression 'TestUtils.equalsAprox(CT16[0].getReal(), real7, 1e-6)' evaluates to <true>
        And expression 'TestUtils.equalsAprox(CT16[0].getImg(), img7, 1e-6)' evaluates to <true>
        Then award 15 points

    Scenario: Verifica se a segunda raiz está correta.
        Given I evaluate 'Complexo C18 = C16[1]'
        And I get field 'real' value in super class of <C18> save in <real8>
        And I get field 'img' value in super class of <C18> save in <img8>
        And expression 'TestUtils.equalsAprox(CT16[1].getReal(), real8, 1e-6)' evaluates to <true>
        And expression 'TestUtils.equalsAprox(CT16[1].getImg(), img8, 1e-6)' evaluates to <true>
        Then award 15 points

    Scenario: Verifica se o método raizesEquacao(Complexo, Complexo, Complexo) está corretamente implementado. (30 pontos)
        Given I report 'Avaliando item 9...'
        Given class 'Avaliacao1' exists store class in <mainClass>
        Given class <mainClass> declares 'raizesEquacao(utfpr.ct.dainf.if62c.avaliacao.Complexo,utfpr.ct.dainf.if62c.avaliacao.Complexo,utfpr.ct.dainf.if62c.avaliacao.Complexo)' method save in <reqMethod>
        And member <reqMethod> has 'public' modifier
        And member <reqMethod> has 'static' modifier
        And method <reqMethod> returns type '[Lutfpr.ct.dainf.if62c.avaliacao.Complexo;'
        Then award 5 points
        Given I evaluate 'ComplexoTest CT19 = new ComplexoTest(a1, b1)'
        Given I evaluate 'ComplexoTest CT20 = new ComplexoTest(a2, b2)'
        Given I evaluate 'ComplexoTest CT21 = new ComplexoTest(a3, b3)'
        Given I evaluate 'ComplexoTest[] CT22 = ComplexoTest.raizesEquacao(CT19,CT20,CT21)'
        And I evaluate 'Complexo C19 = new Complexo(a1, b1)'
        And I evaluate 'Complexo C20 = new Complexo(a2, b2)'
        And I evaluate 'Complexo C21 = new Complexo(a3, b3)'
        And I evaluate 'Complexo[] C22 = Avaliacao1.raizesEquacao(C19,C20,C21)'
        And expression 'C22.length' evaluates to <2>
        Then award 5 points

    Scenario: Verifica se a primeira raiz da equação está correta.
        Given I evaluate 'Complexo C23 = C22[0]'
        And I get field 'real' value in super class of <C23> save in <real9>
        And I get field 'img' value in super class of <C23> save in <img9>
        And expression 'TestUtils.equalsAprox(CT22[0].getReal(), real9, 1e-6)' evaluates to <true>
        And expression 'TestUtils.equalsAprox(CT22[0].getImg(), img9, 1e-6)' evaluates to <true>
        Then award 10 points

    Scenario: Verifica se a segunda raiz da equação está correta.
        Given I evaluate 'Complexo C24 = C22[1]'
        And I get field 'real' value in super class of <C24> save in <real10>
        And I get field 'img' value in super class of <C24> save in <img10>
        And expression 'TestUtils.equalsAprox(CT22[1].getReal(), real10, 1e-6)' evaluates to <true>
        And expression 'TestUtils.equalsAprox(CT22[1].getImg(), img10, 1e-6)' evaluates to <true>
        Then award 10 points

    Scenario: Verifica se a saída do programa tem os valores esperados (20 pontos)
        Given I report 'Avaliando item 10...'
        Given I set output to <testOut>
        And I evaluate 'Avaliacao1.main(new String[0])'
        And I set output to <default>
        Given I evaluate 'ComplexoTest[] CT23 = ComplexoTest.raizesEquacao(new ComplexoTest(1,0),new ComplexoTest(5,0),new ComplexoTest(4,0))'
        Given I evaluate 'ComplexoTest[] CT24 = ComplexoTest.raizesEquacao(new ComplexoTest(1,0),new ComplexoTest(2,0),new ComplexoTest(5,0))'
        Given I evaluate 'String rex1 = CT23[0].getRegex()'
        Given I evaluate 'String rex2 = CT23[1].getRegex()'
        Given I evaluate 'String rey1 = CT24[0].getRegex()'
        Given I evaluate 'String rey2 = CT24[1].getRegex()'

    Scenario: Verifica se a raiz x1 foi exibida
        Given <testOut> matches regex <rex1> with 'Pattern.DOTALL' option
        Then award 5 points

    Scenario: Verifica se a raiz x2 foi exibida
        Given <testOut> matches regex <rex2> with 'Pattern.DOTALL' option
        Then award 5 points

    Scenario: Verifica se a raiz y1 foi exibida
        Given <testOut> matches regex <rey1> with 'Pattern.DOTALL' option
        Then award 5 points

    Scenario: Verifica se a raiz y2 foi exibida
        Given <testOut> matches regex <rey2> with 'Pattern.DOTALL' option
        Then award 5 points

#        Given class 'utfpr.ct.dainf.if62c.avaliacao.Complexo' exists store class in <complexoClass>
#        And class <complexoClass> has 'public' modifier
#        And I evaluate 'Complexo x = new Complexo(a1, b1)'
#        Then award 5 points
#        Given I get field 'real' value in super class of <x> save in <real1>
#        Given I get field 'img' value in super class of <x> save in <img1>
#        And expression 'real1' evaluates to <a1>
#        And expression 'img1' evaluates to <b1>
#        Then award 5 points

#    Scenario: Verifica se a classe Pratica43 existe no pacote padrão e contem o método main (10 pontos)
#        Given I report 'Iniciando avaliação...'
#        Given I report 'Avaliando item 3...'
#        Given class 'Pratica43' exists store class in <mainClass>
#        And class <mainClass> has 'public' modifier
#        Then award 5 points
#        Given class <mainClass> declares 'main(java.lang.String[])' method save in <mainMethod>
#        And method <mainMethod> returns type 'void'
#        And member <mainMethod> has 'public' modifier
#        And member <mainMethod> has 'static' modifier
#        Then award 5 points
#
#    Scenario: Verifica se a interface Figura existe
#        Given I report 'Avaliando item 4...'
#        Given class 'utfpr.ct.dainf.if62c.pratica.Figura' exists store class in <figuraInterface>
#        And <figuraInterface> is an interface
#        Then award 3 points
#
#    Scenario: Verifica se a interface FiguraComEixos existe e extende Figura
#        Given class 'utfpr.ct.dainf.if62c.pratica.FiguraComEixos' exists store class in <figeixosInterface>
#        And <figeixosInterface> is an interface
#        And class 'utfpr.ct.dainf.if62c.pratica.Figura' is assignable from <figeixosInterface> 
#        Then award 3 points
#
#    Scenario: Verifica se a interface FiguraComEixos declara o método getEixoMaior()
#        Given class <figeixosInterface> declares 'getEixoMaior()' method save in <gemaiorMethod>
#        And method <gemaiorMethod> returns type 'double'
#        Then award 3 points
#
#    Scenario: Verifica se a interface FiguraComEixos declara o método getEixoMenor()
#        Given class <figeixosInterface> declares 'getEixoMenor()' method save in <gememprMethod>
#        And method <gememprMethod> returns type 'double'
#        Then award 3 points
#
#    Scenario: Verifica se a classe Elipse existe e implementa a interface FiguraComEixos
#        Given class 'utfpr.ct.dainf.if62c.pratica.Elipse' exists store class in <elipseClass>
#        Given class 'utfpr.ct.dainf.if62c.pratica.FiguraComEixos' is assignable from <elipseClass> 
#        Then award 4 points
#
#    Scenario: Verifica se a classe Circulo existe e extende Elipse
#        Given class 'utfpr.ct.dainf.if62c.pratica.Circulo' exists store class in <circuloClass>
#        Given class 'utfpr.ct.dainf.if62c.pratica.Elipse' is assignable from <circuloClass>  
#        Then award 4 points
#
#    Scenario: Verifica se a interface FiguraComLados existe
#        Given I report 'Avaliando item 5...'
#        Given class 'utfpr.ct.dainf.if62c.pratica.FiguraComLados' exists store class in <figladosInterface>
#        And <figladosInterface> is an interface
#        Then award 10 points
#
#    Scenario: Verifica se a interface FiguraComLados declara getLadoMenor()
#        Given class <figladosInterface> declares 'getLadoMenor()' method save in <glmenorMethod>
#        And method <glmenorMethod> returns type 'double'
#        Then award 10 points
#
#    Scenario: Verifica se a interface FiguraComLados declara getLadoMaior()
#        Given class <figladosInterface> declares 'getLadoMaior()' method save in <glmaiorMethod>
#        And method <glmaiorMethod> returns type 'double'
#        Then award 10 points
#
#    Scenario: Verifica se a classe Retangulo implementa FiguraComLados
#        Given I report 'Avaliando item 6...'
#        Given class 'utfpr.ct.dainf.if62c.pratica.Retangulo' exists store class in <retanguloClass>
#        And class 'utfpr.ct.dainf.if62c.pratica.FiguraComLados' is assignable from <retanguloClass>
#        Then award 10 points
#
#    Scenario: Verifica se a classe Quadrado implementa FiguraComLados
#        Given class 'utfpr.ct.dainf.if62c.pratica.Quadrado' exists store class in <quadradoClass>
#        Given class 'utfpr.ct.dainf.if62c.pratica.FiguraComLados' is assignable from <quadradoClass>
#        Then award 5 points
#        Given class 'utfpr.ct.dainf.if62c.pratica.Retangulo' is assignable from <quadradoClass>
#        Then award 5 points
#
#    Scenario: Verifica se a classe TrianguloEquilatero implementa FiguraComLados
#        Given class 'utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero' exists store class in <equilateroClass>
#        Given class 'utfpr.ct.dainf.if62c.pratica.FiguraComLados' is assignable from <equilateroClass>
#        Then award 5 points
#        Given class 'utfpr.ct.dainf.if62c.pratica.Retangulo' is assignable from <equilateroClass>
#        Then award 5 points
#

    Scenario: Report final grade.
        Given I report grade formatted as 'FINAL GRADE: %.1f'
