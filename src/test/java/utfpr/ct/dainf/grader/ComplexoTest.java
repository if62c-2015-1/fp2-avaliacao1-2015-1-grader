package utfpr.ct.dainf.grader;

import utfpr.ct.dainf.if62c.avaliacao.*;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Primeira avaliação parcial 2014/2.
 * @author 
 */
public class ComplexoTest {
    private double real;
    private double img;

    public ComplexoTest() {
    }

    public ComplexoTest(double real, double img) {
        this.real = real;
        this.img = img;
    }

    public double getReal() {
        return real;
    }

    public double getImg() {
        return img;
    }
    
    public ComplexoTest soma(ComplexoTest c) {
        return new ComplexoTest(real + c.real, img + c.img);
    }
    
    public ComplexoTest sub(ComplexoTest c) {
        return new ComplexoTest(real - c.real, img - c.img);
    }

    public ComplexoTest prod(ComplexoTest c) {
        return new ComplexoTest(real * c.real - img * c.img,
                img * c.real + real * c.img);
    }
    
    public ComplexoTest prod(double x) {
        return new ComplexoTest(x * real, x * img);
    }
    
    public ComplexoTest div(ComplexoTest c) {
        double d = c.real * c.real + c.img * c.img;
        return new ComplexoTest((real * c.real + img * c.img) / d,
                (c.real * img - real * c.img) / d);
    }
    
    public ComplexoTest[] sqrt() {
        double r = Math.sqrt(real * real + img * img);
        double phi;
        if (real > 0)
            phi = Math.atan(img/real);
        else
            if (real < 0)
                phi = Math.atan(img/real) + Math.PI;
            else { // real = 0
                if (img > 0)
                    phi = Math.PI / 2;
                else
                    if (img < 0)
                        phi = 3 * Math.PI / 2;
                    else
                        phi = 0;
            }
        double rho = Math.sqrt(r);
        ComplexoTest[] raiz = new ComplexoTest[2];
        raiz[0] = new ComplexoTest(rho * Math.cos(phi / 2), rho * Math.sin(phi / 2));
        raiz[1] = new ComplexoTest(rho * Math.cos(phi / 2 + Math.PI),
                rho * Math.sin(phi / 2 + Math.PI));
        return raiz;
    }
    
    public String getRegex() {
        return String.format(".*=\\s*%s[0-9.,]*%s[0-9.,]*\\s*i.*",
            String.format("%+f", real).substring(0, 4).replace(".", "\\.").replace("+", "\\+"),
            String.format("%+f", img).substring(0, 4).replace(".", "\\.").replace("+", "\\+"));
    }

    public static ComplexoTest[] raizesEquacao(ComplexoTest a, ComplexoTest b, ComplexoTest c) {
        ComplexoTest[] raizes = new ComplexoTest[2];
        ComplexoTest[] det = b.prod(b).sub(a.prod(4).prod(c)).sqrt();
        raizes[0] = b.prod(-1).soma(det[0]).div(a.prod(2));
        raizes[1] = b.prod(-1).soma(det[1]).div(a.prod(2));
        return raizes;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + (int) (Double.doubleToLongBits(real)
            ^ (Double.doubleToLongBits(real) >>> 32));
        hash = 41 * hash + (int) (Double.doubleToLongBits(img)
            ^ (Double.doubleToLongBits(img) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final ComplexoTest c = (ComplexoTest) obj;
        return obj != null && getClass() == obj.getClass()
            && real == c.real && img == c.img;
    }
    
    @Override
    public String toString() {
        return String.format("%+f%+fi", real, img);
    }
}
