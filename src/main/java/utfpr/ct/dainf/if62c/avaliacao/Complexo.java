package utfpr.ct.dainf.if62c.avaliacao;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática

 * @author 
 */
public class Complexo {
    private double real;
    private double img;

    public Complexo() {
    }

    public Complexo(double real, double img) {
        this.real = real;
        this.img = img;
    }

    public double getReal() {
        return real;
    }

    public double getImg() {
        return img;
    }
    
    public Complexo soma(Complexo c) {
        return new Complexo(real + c.real, img + c.img);
    }
    
    public Complexo sub(Complexo c) {
        return new Complexo(real - c.real, img - c.img);
    }

    public Complexo prod(Complexo c) {
        return new Complexo(real * c.real - img * c.img,
                img * c.real + real * c.img);
    }
    
    public Complexo prod(double x) {
        return new Complexo(x * real, x * img);
    }
    
    public Complexo div(Complexo c) {
        double d = c.real * c.real + c.img * c.img;
        return new Complexo((real * c.real + img * c.img) / d,
                (c.real * img - real * c.img) / d);
    }
    
    public Complexo[] sqrt() {
        double r = Math.sqrt(real * real + img * img);
        double phi;
        if (real > 0)
            phi = Math.atan(img/real);
        else
            if (real < 0)
                phi = Math.atan(img/real) + Math.PI;
            else { // real = 0
                if (img > 0)
                    phi = Math.PI / 2;
                else
                    if (img < 0)
                        phi = 3 * Math.PI / 2;
                    else
                        phi = 0;
            }
        double rho = Math.sqrt(r);
        Complexo[] raiz = new Complexo[2];
        raiz[0] = new Complexo(rho * Math.cos(phi / 2), rho * Math.sin(phi / 2));
        raiz[1] = new Complexo(rho * Math.cos(phi / 2 + Math.PI),
                rho * Math.sin(phi / 2 + Math.PI));
        return raiz;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + (int) (Double.doubleToLongBits(real)
            ^ (Double.doubleToLongBits(real) >>> 32));
        hash = 41 * hash + (int) (Double.doubleToLongBits(img)
            ^ (Double.doubleToLongBits(img) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Complexo c = (Complexo) obj;
        return obj != null && getClass() == obj.getClass()
            && real == c.real && img == c.img;
    }

    @Override
    public String toString() {
        return String.format("%+f%+fi", real, img);
    }
}
