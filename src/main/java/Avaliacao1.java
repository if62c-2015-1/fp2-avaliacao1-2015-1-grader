
import utfpr.ct.dainf.if62c.avaliacao.Complexo;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática

* @author 
 */
public class Avaliacao1 {

    public static void main(String[] args) {

        Complexo a = new Complexo(1, 0);
        Complexo b = new Complexo(5, 0);
        Complexo c = new Complexo(4, 0);
        Complexo[] raizes = raizesEquacao(a, b, c);
        Complexo p0 = a.prod(raizes[0].prod(raizes[0])).soma(b.prod(raizes[0])).soma(c);
        Complexo p1 = a.prod(raizes[1].prod(raizes[1])).soma(b.prod(raizes[1])).soma(c);
        System.out.println("x1=" + raizes[0]);
        System.out.println("x2=" + raizes[1]);
        System.out.println("Verificação das raízes:");
        System.out.println("p(x1)=" + p0);
        System.out.println("p(x2)=" + p1);

        a = new Complexo(1, 0);
        b = new Complexo(2, 0);
        c = new Complexo(5, 0);
        raizes = raizesEquacao(a, b, c);
        p0 = a.prod(raizes[0].prod(raizes[0])).soma(b.prod(raizes[0])).soma(c);
        p1 = a.prod(raizes[1].prod(raizes[1])).soma(b.prod(raizes[1])).soma(c);
        System.out.println("y1=" + raizes[0]);
        System.out.println("y2=" + raizes[1]);
        System.out.println("Verificação das raízes:");
        System.out.println("p(y1)=" + p0);
        System.out.println("p(y2)=" + p1);
    }
    
    public static Complexo[] raizesEquacao(Complexo a, Complexo b, Complexo c) {
        Complexo[] raizes = new Complexo[2];
        Complexo[] det = b.prod(b).sub(a.prod(4).prod(c)).sqrt();
        raizes[0] = b.prod(-1).soma(det[0]).div(a.prod(2));
        raizes[1] = b.prod(-1).soma(det[1]).div(a.prod(2));
        return raizes;
    }
    
}
